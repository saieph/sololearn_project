﻿using System;

namespace sololearn_project
{
    class Loops
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                    continue;

                Console.WriteLine(i);
            }
        }
    }
}
